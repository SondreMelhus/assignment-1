# Assignment 1 : RPG Characters


## Goal

**This project is created as a solution to Assignment 1 : RPG Characters. Task given by Livinus Obiora Nweke, lecturer at Noroff University College, Full-Stack Java Development course.**

The goal of the assignment is to build a console application in Java, that allows the user to create a RPG character inspired by Diablo 3. According to the requirments of the assignment a user should be able to:

- Create a player character, by giving a name and choosing a playable class. 
   
- Equip weapons and armor, based on equipment restriction determined by the class they chosen and level requirements. 
  
- Level up, increasing a player primary attributes and access to new armor and weaponry.
   
- Equipping armor should add the armors own attributes to the players, increasing their primary attributes.
   
- See their damage per second (DPS), with or without a weapon equipped.
   
- DPS calculation also take into account a classes main primary attribute. Resulting in a 1%* DPS increase per primary attribute point.



## Gameplay
The user starts out the game by creating their playble character in the character creator. The creator takes a name and what class the player wants to play, and creates a playable character with these parameters. After the character creator the user sent to the gameplay screen.

**Options in gameplay screen**

1. Character stats: A character sheet containing name, class, level, base primary attributes, total primary attributes and their current damage per second.

2. Equipment: A equipment UI that lets the user equip different weapons and armor. 

3. Level up: Lets the player level up their character, increasing primary attributes and unlocking new armor and weapons.

4. Exit: Lets the player exit the game. (Save functionality has not been implemented).

## Description

### Character
At level 1 a character has a predefined primary attribute distrubtion, based on what class they are. 

**Class starting attribute distribution:**

- Mage    (Strength: 1, Dexterity: 1, Intellegence: 8)
 
- Ranger  (Strength: 1, Dexterity: 7, Intellegence: 1)
 
- Rogue   (Strength: 2, Dexterity: 6, Intellegence: 1)
 
- Warrior (Strength: 5, Dexterity: 2, Intellegence: 1)

 In the game menu there is a option to show your character sheet which shows your name, class, current level, base primary attributes, total primary attributes and your damage per second.As the character levels up their primary attributes are further increased according to a attribute template dependent on the players class.

 **Class level up attribute distribution:**

- Mage    (Strength: 1, Dexterity: 1, Intellegence: 5)
 
- Ranger  (Strength: 1, Dexterity: 5, Intellegence: 1)
 
- Rogue   (Strength: 1, Dexterity: 4, Intellegence: 1)
 
- Warrior (Strength: 3, Dexterity: 2, Intellegence: 1)

### Equipment
The player starts out the game with no items equipped, but by navigating to the equipment menu you can equip different piece of armor and weapons. Each equipable item is assigned a type that restricts what classes can equip the item.

**Class equippable armor:**

- Mage: Cloth
- Ranger: Leather
- Rogue: Leather and mail
- Warrior: Mail and plate

**Class equippable weapons:**

- Mage: Staff and wand
- Ranger: Bow
- Rogue: Dagger and sword
- Warrior: Axe, hammer and sword

If the player tries to equip a armor or weapon that is not part of their equippable weapon types, then the equipItem() method will throw a **InvalidArmorException** or a **InvalidWeaponException**, that will tell the player why they cant equip that item.

Each weapon has a **damage value** and **attacks per second value**, that is combined to calculate the weapons damage per second **(DPS = (damage * attacks per second))**. The DPS then scales further with a players total primary attribute using the following formula, **character DPS = (dps * (1 + total primary attribute / 100))**. This formula also makes it so that a players total dps increase by 1% per primary attribute point. Your primary scaling attribute is dependent on what class you play.

**Class primary scaling attribute:**

- Mage: Intelligence
- Ranger: Dexterity
- Rogue: Dexterity
- Warrior: Strength

## Installation
- Download the repository using SSH key
- Open the project in IntelliJ or any other IDE supporting Java compilation
- Run Main

## Unit testing
To run the unit tests:
- Open the project in IntelliJ or any other IDE supportin Java compilation
- Navigate to the test directory in src
- Right-click the directory and press Run 'All Tests'
- Each test need to return a green check mark to pass the test

## Project status
Finished.

## License
This project is open-source. You are free to use any of the code in your own projects, as long as the work is credited.

## Authors and acknowledgment
**Code author:** Sondre Melhus

**Assignment given by:** Livinus Obiora Nweke, Lecturer at Noroff University College
