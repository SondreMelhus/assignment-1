
//Imports classes from the project that is need for this Class
import no.experis.game.gamelogic.GameLogic;

//This class is used to store the entry-point into the game code
public class Main {

    //Main function
    public static void main(String[] args) {

        GameLogic game = new GameLogic();

        //While loop used to loop the character creator as long as no playable class has been chosen
        while (game.getPlayerClass() == 0) {
            game.createHero();
        }
        //While loop used to loop the idleGameplay as long as the user has not entered the command to exit the game
        while (!game.getExitGame()) {
           game.idleGameplay();
        }










    }

}
