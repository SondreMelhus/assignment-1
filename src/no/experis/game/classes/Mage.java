package no.experis.game.classes;

import no.experis.game.character.EquipableEquipment;
import no.experis.game.character.Hero;
import no.experis.game.classes.classesEnum.PlayerClass;
import no.experis.game.equipment.Weapon;
import no.experis.game.equipment.itemEnums.EquipmentSlot;
import no.experis.game.equipment.itemEnums.ArmorType;
import no.experis.game.equipment.itemEnums.WeaponType;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

//Class that defines a Mage object. Mage is an extension of a Hero
public class Mage extends Hero {

    //Used to store the WeaponTypes a Mage can equip
    private final ArrayList<WeaponType> weaponTypes = new ArrayList<WeaponType>();

    //Used to store the ArmorTypes a Mage can equip
    private final ArrayList<ArmorType> armorTypes = new ArrayList<ArmorType>();

    //Uses weaponTypes and armorTypes to define a Mages equipable equipment
    private final EquipableEquipment equipableEquipment = new EquipableEquipment(weaponTypes, armorTypes);


    //Constructor
    public Mage (String name) {
        super(name);

        //Sets the starting attributes of a Mage
        this.basePrimaryAttribute.updateAttributes(1,1,8);
        this.totalPrimaryAttribute.updateAttributes(1,1,8);

        //Adds all weapon and armor types a Mage can equip
        this.weaponTypes.add(WeaponType.Staff);
        this.weaponTypes.add(WeaponType.Wand);
        this.armorTypes.add(ArmorType.Cloth);
        this.setEquipableEquipment(equipableEquipment);

        //Sets the PlayerClass in the parent Hero to Mage
        this.setPlayerClass(PlayerClass.Mage);
    }

    //Overrides the levelUp method from Hero, so that it uses the correct attributes for Mage
    @Override
    public void levelUp() {
        int level = this.getLevel();
        this.setLevel(level + 1);

        //Sets the new attribute values
        int strength = this.basePrimaryAttribute.getStrength() + 1;
        int dexterity = this.basePrimaryAttribute.getDexterity() + 1;
        int intelligence = this.basePrimaryAttribute.getIntelligence() + 5;

        //Uses the updateAttributes() method to update
        //base- and totalPrimaryAttribute with the values given above
        this.basePrimaryAttribute.updateAttributes(strength, dexterity, intelligence);
        this.totalPrimaryAttribute.updateAttributes(strength, dexterity, intelligence);
    }

    //Overrides the calculateDPS method from Hero
    //Uses the equipped weapons DPS variable to calculate a players DPS with attribute scaling
    @Override
    public double calculateDPS() {

        //Checks if the Mage has a weapon equipped, if not DPS is 1 * (attribute scaling)
        if (this.getEquipped().get(EquipmentSlot.Weapon) == null) {
            return BigDecimal.valueOf(1.0 * (1.0 + (Double.valueOf(this.getTotalPrimaryAttribute().getIntelligence()))
                                                      /100.0)).setScale(2,RoundingMode.CEILING).doubleValue();
        } else {

            //If the Mage has a weapon equipped, it uses its DPS * (attribute scaling)
            Weapon weapon = (Weapon) this.getEquipped().get(EquipmentSlot.Weapon);
            return BigDecimal.valueOf(weapon.getDps() * (1.0 + (Double.valueOf
                                                          (this.getTotalPrimaryAttribute().getIntelligence())) /100.0)).
                                                          setScale(2, RoundingMode.CEILING).doubleValue();
        }
    }
}
