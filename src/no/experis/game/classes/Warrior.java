package no.experis.game.classes;

import no.experis.game.character.EquipableEquipment;
import no.experis.game.character.Hero;
import no.experis.game.classes.classesEnum.PlayerClass;
import no.experis.game.equipment.Weapon;
import no.experis.game.equipment.itemEnums.EquipmentSlot;
import no.experis.game.equipment.itemEnums.ArmorType;
import no.experis.game.equipment.itemEnums.WeaponType;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

//Class that defines a Warrior object. Warrior is an extension of a Hero
public class Warrior extends Hero {

    //Used to store the WeaponTypes a Warrior can equip
    private final ArrayList<WeaponType> weaponTypes = new ArrayList<WeaponType>();

    //Used to store the ArmorTypes a Warrior can equip
    private final ArrayList<ArmorType> armorTypes = new ArrayList<ArmorType>();

    //Uses weaponTypes and armorTypes to define a Warriors equippable equipment
    private final EquipableEquipment equipableEquipment = new EquipableEquipment(weaponTypes, armorTypes);


    //Constructor
    public Warrior (String name) {
        super(name);

        //Sets the starting attributes of a Warrior
        this.basePrimaryAttribute.updateAttributes(5,2,1);
        this.totalPrimaryAttribute.updateAttributes(5,2,1);

        //Adds all weapon and armor types a Warrior can equip
        this.weaponTypes.add(WeaponType.Axe);
        this.weaponTypes.add(WeaponType.Hammer);
        this.weaponTypes.add(WeaponType.Sword);
        this.armorTypes.add(ArmorType.Mail);
        this.armorTypes.add(ArmorType.Plate);
        this.setEquipableEquipment(equipableEquipment);

        //Sets the PlayerClass in the parent Hero to Warrior
        this.setPlayerClass(PlayerClass.Warrior);
    }

    //Overrides the levelUp method from Hero, so that it uses the correct attributes for Warrior
    @Override
    public void levelUp() {
        int level = this.getLevel();
        this.setLevel(level + 1);

        //Sets the new attribute values
        int strength = this.basePrimaryAttribute.getStrength() + 3;
        int dexterity = this.basePrimaryAttribute.getDexterity() + 2;
        int intellegence = this.basePrimaryAttribute.getIntelligence() + 1;

        //Uses the update attributes method from PrimaryAttributes to update
        //basePrimaryAttribute and totalPrimaryAttribute with the values given above
        this.basePrimaryAttribute.updateAttributes(strength, dexterity, intellegence);
        this.totalPrimaryAttribute.updateAttributes(strength, dexterity, intellegence);
    }

    //Overrides the calculateDPS method from Hero
    //Uses the equiped weapons DPS variable to calculate a players DPS with attribute scaling
    @Override
    public double calculateDPS() {

        //Checks if the Warrior has a weapon equipped, if not DPS is 1 * (attribute scaling)
        if (this.getEquipped().get(EquipmentSlot.Weapon) == null) {
            return BigDecimal.valueOf(1.0 * (1.0 + (Double.valueOf(this.getTotalPrimaryAttribute().getStrength()))
                                                  /100.0)).setScale(2,RoundingMode.CEILING).doubleValue();
        } else {

            //If the Warrior has a weapon equipped, it uses its DPS * (attribute scaling)
            Weapon weapon = (Weapon) this.getEquipped().get(EquipmentSlot.Weapon);
            return BigDecimal.valueOf(weapon.getDps() * (1.0 + (Double.valueOf(this.getTotalPrimaryAttribute().
                              getStrength())) /100.0)).setScale(2, RoundingMode.CEILING).doubleValue();
        }
    }
}
