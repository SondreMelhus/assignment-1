package no.experis.game.classes;

import no.experis.game.character.EquipableEquipment;
import no.experis.game.character.Hero;
import no.experis.game.classes.classesEnum.PlayerClass;
import no.experis.game.equipment.Weapon;
import no.experis.game.equipment.itemEnums.EquipmentSlot;
import no.experis.game.equipment.itemEnums.ArmorType;
import no.experis.game.equipment.itemEnums.WeaponType;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

//Class that defines a Rogue object. Rogue is an extension of a Hero
public class Rogue extends Hero {

    //Used to store the WeaponTypes a Rogue can equip
    private final ArrayList<WeaponType> weaponTypes = new ArrayList<WeaponType>();

    //Used to store the ArmorTypes a Rogue can equip
    private final ArrayList<ArmorType> armorTypes = new ArrayList<ArmorType>();

    //Uses weaponTypes and armorTypes to define a Rogues equipable equipment
    private final EquipableEquipment equipableEquipment = new EquipableEquipment(weaponTypes, armorTypes);


    //Constructor
    public Rogue (String name) {
        super(name);

        //Sets the starting attributes of a Rogue
        this.basePrimaryAttribute.updateAttributes(2,6,1);
        this.totalPrimaryAttribute.updateAttributes(2,6,1);

        //Adds all weapon and armor types a Rogue can equip
        this.weaponTypes.add(WeaponType.Dagger);
        this.weaponTypes.add(WeaponType.Sword);
        this.armorTypes.add(ArmorType.Leather);
        this.armorTypes.add(ArmorType.Mail);
        this.setEquipableEquipment(equipableEquipment);

        //Sets the PlayerClass in the parent Hero to Rogue
        this.setPlayerClass(PlayerClass.Rogue);
    }

    //Overrides the levelUp method from Hero, so that it uses the correct attributes for Rogue
    @Override
    public void levelUp() {
        int level = this.getLevel();
        this.setLevel(level + 1);

        //Sets the new attribute values
        int strength = this.basePrimaryAttribute.getStrength() + 1;
        int dexterity = this.basePrimaryAttribute.getDexterity() + 4;
        int intellegence = this.basePrimaryAttribute.getIntelligence() + 1;

        //Uses the update attributes method from PrimaryAttributes to update
        //basePrimaryAttribute and totalPrimaryAttribute with the values given above
        this.basePrimaryAttribute.updateAttributes(strength, dexterity, intellegence);
        this.totalPrimaryAttribute.updateAttributes(strength, dexterity, intellegence);
    }

    //Overrides the calculateDPS method from Hero
    //Uses the equiped weapons DPS variable to calculate a players DPS with attribute scaling
    @Override
    public double calculateDPS() {

        //Checks if the Rogue has a weapon equipped, if not DPS is 1 * (attribute scaling)
        if (this.getEquipped().get(EquipmentSlot.Weapon) == null) {
            return BigDecimal.valueOf(1.0 * (1.0 + (Double.valueOf(this.getTotalPrimaryAttribute().getDexterity()))
                                                    /100.0)).setScale(2,RoundingMode.CEILING).doubleValue();
        } else {

            //If the Rogue has a weapon equipped, it uses its DPS * (attribute scaling)
            Weapon weapon = (Weapon) this.getEquipped().get(EquipmentSlot.Weapon);
            return BigDecimal.valueOf(weapon.getDps() * (1.0 + (Double.valueOf(this.getTotalPrimaryAttribute().
                             getDexterity())) /100.0)).setScale(2, RoundingMode.CEILING).doubleValue();
        }
    }
}
