//This enum is part of the classesEnum package
package no.experis.game.classes.classesEnum;

//Enum used to store a players class in Hero
public enum PlayerClass {
    Mage,
    Ranger,
    Rogue,
    Warrior
}
