package no.experis.game.gamelogic;

import no.experis.game.character.Player;
import no.experis.game.customExceptions.GameInputException;
import no.experis.game.customExceptions.InvalidArmorException;
import no.experis.game.customExceptions.InvalidWeaponException;
import no.experis.game.equipment.Armor;
import no.experis.game.equipment.EquipmentPool;
import no.experis.game.equipment.Weapon;
import no.experis.game.equipment.itemEnums.EquipmentSlot;

import java.util.Scanner;

//Class that runs the game logic, allowing the user to interact with the game
public class GameLogic {

    //Scanner used to get inputs from the user
    private final Scanner myScanner = new Scanner(System.in);

    //Used to generate a EquipmentPool of all available weapons
    private final EquipmentPool items = new EquipmentPool();

    //Used to track and controll a player character
    private final Player player = new Player();

    //Used to set and track a characters class, used as a key in <Integer, Hero> hashMap on the player object
    private int playerClass = 0;

    //Boolean that tracks when the user wants to exit the game
    private boolean exitGame = false;

    //Method used to check if a given string only consist digits
    public void handleInput (String s) throws GameInputException {

        //For each character in the string check if it is a digit
        for (Character c : s.toCharArray()) {
            if (!Character.isDigit(c)) {

                //Throws a GameInputException if the character is not a digit
                throw new GameInputException("Invalid user input");
            }
        }
    }

    //Method used to create a new player character
    public void createHero() {

        //Game introduction
        System.out.println("Hello Adventurer! What is your name?");
        System.out.println();

        //Stores scanner input so it can be used to name a player character
        String name = myScanner.nextLine();
        System.out.println();

        System.out.println("Nice to meet you " + name + "! What class would you like to play?");
        System.out.println();

        //Prints all the available classes, with their start attributes
        for (int i = 1; i <= player.getPlayerHeros().size(); i++) {
            System.out.println("   " + i + ". " + player.getPlayerHeros().get(i).getPlayerClass()
                    + " - Starting attributes: " +  player.getPlayerHeros().get(i).getBasePrimaryAttribute().
                    textAttributes());
        }
        System.out.println();


        //Try-catch to check that a users input is only digits
        try {

            //Stores user input as a String
            String input = myScanner.nextLine();

            //Checks that the input is only digits, otherwise throw a GameInputException.
            handleInput(input);

            //Parse the input to an integer
            playerClass = Integer.parseInt(input);

            //Use the parsed input as a key to set the name of the playable class we want to use
            player.getPlayerHeros().get(playerClass).setName(name);
            System.out.println("---------------------------------------------------------------------------------------------------------------------");
            System.out.println();
        } catch (GameInputException e) {
            System.out.println();
            System.out.println(e);
            System.out.println();
        }
    }


    //Method used to handle the equipment UI
    //Prints all available equipment from the weapon and armor pool, and equips an item based on user input
    public void handleEquipmentUI() {

        //Used to track equipment options
        int exitIndex = items.getItemPool().size() + 1;

        //Used to track if a user wants to go back to the idle gameplay screen
        boolean done = false;

        //Generates a "pool" of all available items
        items.generateItemPool();

        System.out.println("Inventory:");
        System.out.println("Type the number of the item you want to equip");
        System.out.println();

        //Display the item pool. For each weapon in the weapon pool print the items info
        for (int i = 1; i <= items.getItemPool().size(); i++) {

            //The string builder used to tidy up the weapon display, making it easier to read
            StringBuilder builder = new StringBuilder(items.getItemPool().get(i).getName());

            //Checks if the length of the current item name is longer than 28 (the longest name of all the items)
            if (builder.length() < 28) {

                //The String builder adds necessary spaces to the end of
                // the name,so that each weapon name is the same length
                builder.append("                                                  ", builder.length(), 28);
            }

            //Checks if the items equipment slot is a weapon, cast the item to a Weapon
            // and print the weapons equipment option number, name, slot, required level and weapon type
            if (items.getItemPool().get(i).getEquipmentSlot() == EquipmentSlot.Weapon) {
                Weapon weapon = (Weapon) items.getItemPool().get(i);

                System.out.println("   " + i + " - " + builder + "  -  Slot: " + weapon.getEquipmentSlot() +
                        "  -  Required level: " + weapon.getRequiredLevel() + " - Type: " + weapon.getWeaponType());
            } else {

                //If the items equipment slot is not a weapon, cast the item to a Armor
                // and print the armors equipment option number, name, slot, required level and armor type
                Armor armor = (Armor) items.getItemPool().get(i);

                System.out.println("   " + i + " - " + builder + "  -  Slot: " + armor.getEquipmentSlot() +
                        "  -  Required level: " + armor.getRequiredLevel() + " - Type: " + armor.getArmorType());
            }
            exitIndex = i + 1;
        }
        System.out.println();

        //Prints the number user have to input to return to idle game screen
        System.out.println(exitIndex + ". Back");

        //While the user has not chosen to exit the equipmentUI check for user inputs,
        // handle the input with GameInputException and then parse the input to a int
        while (!done) {
            int input = 0;
            try {
                String command = myScanner.next();
                handleInput(command);
                input = Integer.parseInt(command);
            } catch (GameInputException e) {
                System.out.println();
                System.out.println(e);
                System.out.println();
            }

            //Checks of the user input int is less than the exit equipmentUI option
            if (input < exitIndex) {

                //Use the user input as a key to get the item the user chose and check if its a weapon
                if (items.getItemPool().get(input).getEquipmentSlot() == EquipmentSlot.Weapon) {

                    //Try to equip the weapon, throws InvalidWeaponException if the player cant equip it
                    try {
                        player.getPlayerHeros().get(playerClass).equipWeapon(items.getItemPool().get(input));
                    } catch (InvalidWeaponException e) {
                        System.out.println(e);
                    }
                } else {

                    //Try to equip the armor the user chose, throws InvalidArmorException if the player cant equip it
                    try {
                        player.getPlayerHeros().get(playerClass).equipArmor(items.getItemPool().get(input));
                    } catch (InvalidArmorException e) {
                        System.out.println(e);
                    }
                }
            } else {
                done = true;    //Ends the loop that lets the user equip multiple items
            }
        }


    }
    //Method that is used to handle the different reactions to input during idle gameplay
    //Consists of a switch that runs a case based on the input value of the method
    public void handleIdleUI (int i) {

        switch (i) {
            //Case that lets the user check their stats by using the printHeroInfo method from the Hero class
            case 1:
                System.out.println();

                //Uses the pringHeroInfo method to print the name, level, class, attribute points and DPS
                player.getPlayerHeros().get(playerClass).printHeroInfo();
                System.out.println();
                break;

            //Case that lets the user enter the equipmentUI, where they can see all available equipment, and equip items
            case 2:
                System.out.println();
                System.out.println();

                //Uses the handleEquipmentUI function to run the equipment UI
                handleEquipmentUI();
                System.out.println();
                break;

            //Case that lets the used level up their character
            case 3:
                System.out.println();

                //Uses the levelUp method that each playable class overrides from Hero
                player.getPlayerHeros().get(playerClass).levelUp();
                System.out.println("Congratulations! You are now level " + player.getPlayerHeros().get(playerClass).
                        getLevel() + " " + player.getPlayerHeros().get(playerClass).getPlayerClass());
                System.out.println("Your new base primary attributes are: " + player.getPlayerHeros().get(playerClass).
                        getBasePrimaryAttribute().textAttributes());
                System.out.println("Your new total primary attributes are: " + player.getPlayerHeros().get(playerClass).
                        getTotalPrimaryAttribute().textAttributes());
                System.out.println("DPS: " + player.getPlayerHeros().get(playerClass).calculateDPS());
                System.out.println();
                break;

            //Default case that lets the user end their game session
            default:
                System.out.println();
                System.out.println("Goodbye for now " + player.getPlayerHeros().get(playerClass).getName() + "!");
                this.setExitGame(true);
                break;
        }
    }

    //Method used to print the idle gameplay ui, and call the handleUI method
    public void idleGameplay() {

        //Prints the idle gameplay menu, that shows the user different input options
        System.out.println("What would you like to do?");
        System.out.println("   1. Check your stats");
        System.out.println("   2. Check your equipment");
        System.out.println("   3. Level up");
        System.out.println("   4. Exit the game");
        System.out.println();

        //Try-catch that tries to take a input from the user, checksvthat it is valid
        // and then parses it into an integer command that is fed to handleIdleUI
        try {
            String command = myScanner.next();
            handleInput(command);
            handleIdleUI(Integer.parseInt(command));
        } catch (GameInputException e) {
            System.out.println();
            System.out.println(e);
            System.out.println();

        }

    }


    //Getters and setters
    public void setExitGame(boolean exitGame) {
        this.exitGame = exitGame;
    }

    public boolean getExitGame() {
        return this.exitGame;
    }

    public int getPlayerClass() {
        return playerClass;
    }
}
