package no.experis.game.customExceptions;

//Class that extends to Exception.
//This exception is used to handle exceptions that could occur during the equipping of Weapons
public class InvalidWeaponException extends Exception {
    public InvalidWeaponException(String message){super(message); }
}

