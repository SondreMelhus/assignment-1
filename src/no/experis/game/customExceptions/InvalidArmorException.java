package no.experis.game.customExceptions;

//Class that extends to Exception.
//This exception is used to handle exceptions that could occur during the equipping of Armor
public class InvalidArmorException extends Exception{
    public InvalidArmorException(String message){super(message); }
}
