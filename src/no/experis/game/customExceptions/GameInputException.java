package no.experis.game.customExceptions;

//Class that extends to Exception.
//Custom exception used to handle exceptions that could occur during handling of user inputs
public class GameInputException extends Exception {

    public GameInputException (String message) {super(message); }
}
