//This enum is part of the itemEnums package
package no.experis.game.equipment.itemEnums;

//Enum used to store an Item's equipment slot
public enum EquipmentSlot {
    Head,
    Body,
    Legs,
    Weapon
}
