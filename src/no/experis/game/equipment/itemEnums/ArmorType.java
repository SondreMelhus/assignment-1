//This enum is part of the itemEnums package
package no.experis.game.equipment.itemEnums;

//Enum used to store armor type in Armor
public enum ArmorType {
    Cloth,
    Leather,
    Mail,
    Plate
}
