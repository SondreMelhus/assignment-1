//This enum is part of the itemEnums package
package no.experis.game.equipment.itemEnums;

//Used to store the weapon type in Weapon
public enum WeaponType {
    Axe,
    Bow,
    Dagger,
    Hammer,
    Staff,
    Sword,
    Wand
}
