package no.experis.game.equipment;

import no.experis.game.equipment.itemEnums.EquipmentSlot;

//Class that defines a Item object. Is used as super in Armor and Weapon class.
public abstract class Item {

    //Used to store the name of an Item
    private String name;

    //Used to store the required level of an Item
    private int requiredLevel;

    //Used to store what slot the Item goes in
    private EquipmentSlot equipmentSlot;

    //Constructor
    public Item(String name, int requiredLevel, EquipmentSlot equipmentSlot) {
        this.name = name;
        this.requiredLevel = requiredLevel;
        this.equipmentSlot = equipmentSlot;
    }

    //Getter and setter
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRequiredLevel() {
        return requiredLevel;
    }

    public EquipmentSlot getEquipmentSlot() {
        return equipmentSlot;
    }

}
