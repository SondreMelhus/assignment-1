//This class is part of the equipment package
package no.experis.game.equipment;

//Imports classes from the project that is need for this Class

import no.experis.game.character.PrimaryAttribute;
import no.experis.game.equipment.itemEnums.ArmorType;
import no.experis.game.equipment.itemEnums.EquipmentSlot;
import no.experis.game.equipment.itemEnums.WeaponType;

import java.util.HashMap;

//Class that defines a "pool" containing all equippable armor and weapons
public class EquipmentPool {

    //HashMap used to store armor and weapons, uses a integer as key
    private final HashMap<Integer, Item> items = new HashMap<>();

    //Intellegence templates used to assign attributes to an Item
    private final PrimaryAttribute intTemplate1 = new PrimaryAttribute(1, 1, 8);
    private final PrimaryAttribute intTemplate2 = new PrimaryAttribute(1, 1, 15);
    private final PrimaryAttribute intTemplate3 = new PrimaryAttribute(1, 1, 30);


    //Dexterity templates used to assign attributes to an Item
    private final PrimaryAttribute dexTemplate1 = new PrimaryAttribute(1, 8, 1);
    private final PrimaryAttribute dexTemplate2 = new PrimaryAttribute(1, 15, 1);
    private final PrimaryAttribute dexTemplate3 = new PrimaryAttribute(1, 30, 1);


    //Strength templates used to assign attributes to an Item
    private final PrimaryAttribute strTemplate1 = new PrimaryAttribute(8, 1, 1);
    private final PrimaryAttribute strTemplate2 = new PrimaryAttribute(15, 1, 1);
    private final PrimaryAttribute strTemplate3 = new PrimaryAttribute(30, 1, 1);

    //Instantiate all equippable armors and weapons

    //Weapons
    //Signatur: (String name, int reqLvl, WeaponType weaponType,
    //EquipmentSlot equipmentSlot, int damage, double attackSpeed)

    //Axes
    protected Weapon woodcuttersAxe = new Weapon("Woodcutter's axe", 1, WeaponType.Axe,
            EquipmentSlot.Weapon, 5, 0.9);
    protected Weapon steelAxe = new Weapon("Steel axe", 5, WeaponType.Axe,
            EquipmentSlot.Weapon, 18, 0.7);


    //Daggers
    protected Weapon ironDagger = new Weapon("Iron dagger", 1, WeaponType.Dagger,
            EquipmentSlot.Weapon, 14, 2.2);
    protected Weapon stilleto = new Weapon("Stilleto", 6, WeaponType.Dagger,
            EquipmentSlot.Weapon, 20, 2.4);


    //Swords
    protected Weapon ironSword = new Weapon("Iron sword", 1, WeaponType.Sword,
            EquipmentSlot.Weapon, 12, 1.2);
    protected Weapon knightSword = new Weapon("Knight sword", 5, WeaponType.Sword,
            EquipmentSlot.Weapon, 18, 1.4);


    //Hammers
    protected Weapon blacksmithHammer = new Weapon("Blacksmith hammer", 1, WeaponType.Hammer,
            EquipmentSlot.Weapon, 18, 0.8);
    protected Weapon cavelryMace = new Weapon("Cavelry mace", 9, WeaponType.Hammer,
            EquipmentSlot.Weapon, 28, 1);


    //Bows
    protected Weapon woodBow = new Weapon("Wood bow", 1, WeaponType.Bow, EquipmentSlot.Weapon,
            8, 2);
    protected Weapon compositeBow = new Weapon("Composite bow", 8, WeaponType.Bow,
            EquipmentSlot.Weapon, 18, 2.7);

    //Staffs
    protected Weapon birchStaff = new Weapon("Simple birch staff", 1, WeaponType.Staff,
            EquipmentSlot.Weapon, 15, 0.8);
    protected Weapon staffOfCreation = new Weapon("The staff of Creation", 6, WeaponType.Staff,
            EquipmentSlot.Weapon, 25, 1.1);


    //Wands
    protected Weapon spruceWand = new Weapon("Spruce wand", 1, WeaponType.Wand,
            EquipmentSlot.Weapon, 8, 1.5);
    protected Weapon elderWand = new Weapon("Elder wand", 6, WeaponType.Wand,
            EquipmentSlot.Weapon, 19, 2);


    //Armor
    //Signature: String name, int reqLvl,EquipmentSlot equipmentSlot,
    // ArmorType armorType, int defence

    //Cloth
    protected Armor hoodOfTheArchMage = new Armor("Hood of the Arch-Mage", 1, EquipmentSlot.Head,
            ArmorType.Cloth, 5, intTemplate1);
    protected Armor robesOfTheArchMage = new Armor("Robes of the Arch-Mage", 5, EquipmentSlot.Body,
            ArmorType.Cloth, 20, intTemplate2);
    protected Armor breachesOfTheArchMage = new Armor("Breaches of the Arch-Mage", 3,
            EquipmentSlot.Legs, ArmorType.Cloth, 7, intTemplate1);

    //Leather
    protected Armor mantelOfTheNight = new Armor("Mantel of the Night", 1, EquipmentSlot.Head,
            ArmorType.Leather, 6, dexTemplate1);
    protected Armor breastplateOfTheNight = new Armor("Breastplate of the the Night", 5,
            EquipmentSlot.Body, ArmorType.Leather, 23, dexTemplate2);
    protected Armor breachesOfTheNight = new Armor("Breaches of the Night", 3, EquipmentSlot.Legs,
            ArmorType.Leather, 9, dexTemplate1);

    //Mail
    protected Armor mailHelm = new Armor("Mail helm", 1, EquipmentSlot.Head, ArmorType.Mail, 6,
            strTemplate1);
    protected Armor mailShirt = new Armor("Mail shirt", 5, EquipmentSlot.Body, ArmorType.Mail,
            17, strTemplate2);
    protected Armor mailPants = new Armor("Mail pants", 3, EquipmentSlot.Legs, ArmorType.Mail,
            10, strTemplate1);

    //Plate
    protected Armor ironHelm = new Armor("Iron helmet", 1, EquipmentSlot.Head, ArmorType.Plate,
            6, strTemplate1);
    protected Armor ironPlateChest = new Armor("Iron plate chest", 5, EquipmentSlot.Body,
            ArmorType.Plate, 15, strTemplate2);
    protected Armor ironPlategrieves = new Armor("Iron plate grieves", 3, EquipmentSlot.Legs,
            ArmorType.Plate, 7, strTemplate1);

    //Method that adds all weapond and armor into the itemPool
    public void generateItemPool() {
        //Adding all the weapons to the item pool
        this.items.put(1, ironDagger);
        this.items.put(2, stilleto);
        this.items.put(3, ironSword);
        this.items.put(4, knightSword);
        this.items.put(5, blacksmithHammer);
        this.items.put(6, cavelryMace);
        this.items.put(7, woodcuttersAxe);
        this.items.put(8, steelAxe);
        this.items.put(9, woodBow);
        this.items.put(10, compositeBow);
        this.items.put(11, birchStaff);
        this.items.put(12, staffOfCreation);
        this.items.put(13, spruceWand);
        this.items.put(14, elderWand);

        //Adding all the armor to the item pool
        this.items.put(15, hoodOfTheArchMage);
        this.items.put(16, robesOfTheArchMage);
        this.items.put(17, breachesOfTheArchMage);
        this.items.put(18, mantelOfTheNight);
        this.items.put(19, breastplateOfTheNight);
        this.items.put(20, breachesOfTheNight);
        this.items.put(20, mailHelm);
        this.items.put(21, mailShirt);
        this.items.put(22, mailPants);
        this.items.put(23, ironHelm);
        this.items.put(24, ironPlateChest);
        this.items.put(25, ironPlategrieves);
    }


    //Getter
    public HashMap<Integer, Item> getItemPool() {
        return items;
    }
}
