package no.experis.game.equipment;

import no.experis.game.equipment.itemEnums.EquipmentSlot;
import no.experis.game.equipment.itemEnums.WeaponType;

//Class that defines a Weapon object, with Item as its super
public class Weapon extends Item {

    //Used to store the weapon type
    private WeaponType weaponType;

    //Used to store the damage
    private double damage;

    //Used to store the attack speed
    private double attackSpeed;

    //Used to store the dps (damage * attackSpeed)
    private double dps;


    //Constructor
    public Weapon(String name, int requiredLevel, WeaponType weaponType, EquipmentSlot equipmentSlot, int damage,
                  double attackSpeed) {
        super(name, requiredLevel, equipmentSlot);
        this.weaponType = weaponType;
        this.damage = damage;
        this.attackSpeed = attackSpeed;

        //Sets the dps of this weapon equal to damage * attackSpeed
        this.dps = damage * attackSpeed;
    }


    //Getter and setter
    public WeaponType getWeaponType() {
        return weaponType;
    }

    public void setWeaponType(WeaponType weaponType) {
        this.weaponType = weaponType;
    }

    public double getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    public double getDps() {
        return dps;
    }

    public void setDps(double dps) {
        this.dps = dps;
    }
}
