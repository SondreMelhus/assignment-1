package no.experis.game.equipment;

import no.experis.game.character.PrimaryAttribute;
import no.experis.game.equipment.itemEnums.EquipmentSlot;
import no.experis.game.equipment.itemEnums.ArmorType;

//Class that defines a Armor object, with Item as its super
public class Armor extends Item {

    //Used to store the armor type
    private ArmorType armorType;

    //Used to set an armors defence rating
    private int defence;

    //Used to store the armors primary attributes
    private PrimaryAttribute attributes;


    //Constructor
    public Armor(String name, int requiredLevel, EquipmentSlot equipmentSlot, ArmorType armorType,
                                                         int defence, PrimaryAttribute attributes) {
        super(name, requiredLevel, equipmentSlot);
        this.armorType = armorType;
        this.defence = defence;
        this.attributes = attributes;
    }


    //Getter and setter
    public ArmorType getArmorType() {
        return armorType;
    }

    public void setArmorType(ArmorType armorType) {
        this.armorType = armorType;
    }

    public int getDefence() {
        return defence;
    }

    public void setDefence(int defence) {
        this.defence = defence;
    }
    public PrimaryAttribute getAttributes() {
        return attributes;
    }

    public void setAttributes(PrimaryAttribute attributes) {
        this.attributes = attributes;
    }
}
