package no.experis.game.character;

import no.experis.game.customExceptions.InvalidArmorException;
import no.experis.game.customExceptions.InvalidWeaponException;
import no.experis.game.equipment.Armor;
import no.experis.game.equipment.Item;
import no.experis.game.equipment.Weapon;
import no.experis.game.equipment.itemEnums.EquipmentSlot;
import no.experis.game.classes.classesEnum.PlayerClass;

import java.util.HashMap;

//Class that defines a Hero object. Is used as super in Mage, Ranger, Rogue and Warrior class
public abstract class Hero implements HeroInterface {

    //Used to store the Hero's name
    protected String name;

    //Used to store the Hero's level
    protected int level = 1;

    //Used to store the Hero's base primary attributes
    protected PrimaryAttribute basePrimaryAttribute = new PrimaryAttribute(0,0,0);

    //Used to store the Hero's total primary attributes
    protected PrimaryAttribute totalPrimaryAttribute = new PrimaryAttribute(0,0,0);

    //Used to store all the items the Hero currently has equipped
    private final HashMap<EquipmentSlot, Item> equipped = new HashMap<>();

    //Used to store what type of items the Hero can equip
    protected EquipableEquipment equipableEquipment;

    //Used to store what playable class the Hero is
    protected PlayerClass playerClass;

    //Constructor
    public Hero(String name) {
        this.name = name;
    }


    //Method used to print information about a Hero
    //Prints their name, class, level, base primary attributes, total primary attributes and dps
    public void printHeroInfo() {
        System.out.println("Character sheet:");
        System.out.println("    Name: " + this.name);
        System.out.println("    Class: " + this.playerClass);
        System.out.println("    Level: " + this.level);
        System.out.println("    Base primary attributes: " + this.basePrimaryAttribute.textAttributes());
        System.out.println("    Total primary attributes: " + this.totalPrimaryAttribute.textAttributes());
        System.out.println("    DPS: " + this.calculateDPS());
        System.out.println();
    }


    //Method used to level up a hero
    //Each playable class implements an Overriden version of this method
    public void levelUp() {}


    //Method used to equip a weapon
    //The method takes a Item object, and adds it to the Hero's equipped hashMap.
    public boolean equipWeapon (Item item) throws InvalidWeaponException {
        boolean equipped = false;

        //Start by checking if the given item is null
        if (item != null) {

            //Cast the item object into a weapon object so that we can use Weapon methods on it
            Weapon weapon = (Weapon) item;

            //Check the level requirements of the weapon and weapon type,
            // cast InvalidWeaponException if the Hero doesnt meet the requirements
            if(this.getLevel() < item.getRequiredLevel()) {
                throw new InvalidWeaponException("You are too low level to equip this weapon");
            } else if (!this.equipableEquipment.getEquipableWeapons().contains(weapon.getWeaponType())) {
                    throw new InvalidWeaponException("You cannot equip this weapon type");
            } else {

                //Check if their currently is a weapon equipped, so that
                // we now if we have to use put or replace on the hashMap.
                // We use the wepons equipment slot as tha hashMap key
                if (this.getEquipped().get(EquipmentSlot.Weapon) == null) {
                    this.getEquipped().put(weapon.getEquipmentSlot(), weapon);
                } else {
                    this.getEquipped().replace(weapon.getEquipmentSlot(), weapon);
                }
                equipped = true;
                System.out.println("You have now equipped " + weapon.getName() + " in your " + weapon.getEquipmentSlot()
                                                                                                             + " slot");

            }
        }
        //Return if the weapon was equipped or not
        return equipped;
    }

    //Method used to equip a armor
    //The method takes a Item object, and adds it to the Hero's equipped hashMap.
    public boolean equipArmor (Item item) throws InvalidArmorException {
        boolean equipped = false;

        //Start by checking if the given item is null
        if (item != null) {

            //Cast the item object into an armor object so that we can use Armor methods on it
            Armor armor = (Armor) item;

            //Check the level requirements of the armor and armor type,
            // cast InvalidWeaponException if the Hero doesnt meet the requirements
            if(this.getLevel() < item.getRequiredLevel()) {
                throw new InvalidArmorException("You are too low level to equip this armor");
            } else if (!this.equipableEquipment.getEquipableArmor().contains(armor.getArmorType())) {
                    throw new InvalidArmorException("You cannot equip this armor type");
            } else {

                //Check if their currently is a armor piece equipped in the items equipment slot.
                //If not we use put to add the armor to the equipped hashMap, and add the armors
                //attributes to the Hero's totalPrimaryAttribute
                if (this.getEquipped().get(item.getEquipmentSlot()) == null) {
                    this.getEquipped().put(armor.getEquipmentSlot(), armor);
                    this.getTotalPrimaryAttribute().addItemAttributes(item);
                } else {

                    //If there currently is a armor piece in the equipment slot, we first remove
                    // the old items attributes from Hero's totalPrimaryAttributes, then add the
                    // armor to the equipped hashMap, and add the new attributes to totalPrimaryAttributes
                    this.getTotalPrimaryAttribute().removeItemAttributes(this.equipped.get(item.getEquipmentSlot()));
                    this.getEquipped().replace(armor.getEquipmentSlot(), armor);
                    this.getTotalPrimaryAttribute().addItemAttributes(item);
                }
                equipped = true;
                System.out.println("You have now equipped " + armor.getName() + " in your " + armor.getEquipmentSlot() +
                                                                                                               " slot");
            }

        }
        //Return if the armor was equipped or not
        return equipped;
    }

    //Method used to calculate a Hero's DPS
    //Each playable class implements an Overriden version of this method
    public double calculateDPS() {
        return 1.0;
    }



    //Getters and setters
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public PrimaryAttribute getBasePrimaryAttribute() {
        return basePrimaryAttribute;
    }

    public PrimaryAttribute getTotalPrimaryAttribute() {
        return totalPrimaryAttribute;
    }

    public HashMap<EquipmentSlot, Item> getEquipped() {
        return equipped;
    }

    public void setEquipableEquipment(EquipableEquipment equipableEquipment) {
        this.equipableEquipment = equipableEquipment;
    }

    public PlayerClass getPlayerClass() {
        return playerClass;
    }

    public void setPlayerClass(PlayerClass playerClass) {
        this.playerClass = playerClass;
    }
}
