package no.experis.game.character;

import no.experis.game.classes.Mage;
import no.experis.game.classes.Ranger;
import no.experis.game.classes.Rogue;
import no.experis.game.classes.Warrior;

import java.util.HashMap;

//Class used to create a Player object, which lets us manipulate the playable character during gameplay
public class Player {

    //Instantiating a different playable classes
    private final Mage mage = new Mage("Placeholder Mage");
    private final Ranger ranger = new Ranger ("Placeholder Ranger");
    private final Rogue rogue = new Rogue("Placeholder Rogue");
    private final Warrior warrior = new Warrior("Placeholder Warrior");

    //HashMap used to store all playable classes
    private HashMap<Integer, Hero> playerHeros = new HashMap<>();


    //Constructor
    public Player () {

        //Adding all the playable classes to a hashMap with integer as key
        playerHeros.put(1, mage);
        playerHeros.put(2, ranger);
        playerHeros.put(3, rogue);
        playerHeros.put(4, warrior);
    }

    //Getter
    public HashMap<Integer, Hero> getPlayerHeros() {
        return playerHeros;
    }
}
