package no.experis.game.character;

import no.experis.game.equipment.Armor;
import no.experis.game.equipment.Item;

//Class used to store PrimaryAttributes. Used in conjuncting with Hero and Armor
public class PrimaryAttribute {

    //Variables used to store the three primary attributes
    private int strength;
    private int dexterity;
    private int intelligence;


    //Constructor
    public PrimaryAttribute(int strength, int dexterity, int intelligence) {
        this.strength = strength;
        this.dexterity = dexterity;
        this.intelligence = intelligence;
    }


    //Method used to print the primary attributes
    public String textAttributes() {
        return ("Strength: " + this.strength +" - Dexterity: " + this.dexterity + " - Intellegence: " + this.intelligence);
    }

    //Method used to update the primary attributes.
    public void updateAttributes(int strength, int dexterity, int intellegence) {
        this.setStrength(strength);
        this.setDexterity(dexterity);
        this.setIntelligence(intellegence);
    }


    //Method used to add an armors attribute to a Hero
    public void addItemAttributes(Item item) {
        Armor armor = (Armor) item;

        this.setStrength(this.getStrength() + armor.getAttributes().getStrength());
        this.setDexterity(this.getDexterity() + armor.getAttributes().getDexterity());
        this.setIntelligence(this.getIntelligence() + armor.getAttributes().getIntelligence());
    }


    //Method used to remove an armors attributes from a Hero
    public void removeItemAttributes(Item item) {
        Armor armor = (Armor) item;
        this.setStrength(this.getStrength() - armor.getAttributes().getStrength());
        this.setDexterity(this.getDexterity() - armor.getAttributes().getDexterity());
        this.setIntelligence(this.getIntelligence() - armor.getAttributes().getIntelligence());
    }


    //Getters and setters
    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public int getDexterity() {
        return dexterity;
    }

    public void setDexterity(int dexterity) {
        this.dexterity = dexterity;
    }

    public int getIntelligence() {
        return intelligence;
    }

    public void setIntelligence(int intelligence) {
        this.intelligence = intelligence;
    }
}
