package no.experis.game.character;

import no.experis.game.equipment.itemEnums.ArmorType;
import no.experis.game.equipment.itemEnums.WeaponType;

import java.util.ArrayList;

//Class used to store arrayLists containing the WeaponTypes and ArmorTypes a character can equip
public class EquipableEquipment {

    //Stores all the ArmorTypes a character can equip
    private final ArrayList<ArmorType> equipableArmor;

    //Stores all the WeaponTypes a character can equip
    private final ArrayList<WeaponType> equipableWeapons;


    //Constructor
    public EquipableEquipment(ArrayList<WeaponType> equipableWeapons, ArrayList<ArmorType> equipableArmor) {
        this.equipableWeapons = equipableWeapons;
        this.equipableArmor = equipableArmor;
    }


    //Getters and setters
    public ArrayList<WeaponType> getEquipableWeapons() {
        return equipableWeapons;
    }

    public ArrayList<ArmorType> getEquipableArmor() {
        return equipableArmor;
    }
}
