package no.experis.game.character;

//Interface used to define required methods for all classes implementing the interface
public interface HeroInterface {

    void levelUp();

}
