package no.experis.game.character;

import no.experis.game.classes.Warrior;
import no.experis.game.customExceptions.InvalidArmorException;
import no.experis.game.customExceptions.InvalidWeaponException;
import no.experis.game.equipment.Armor;
import no.experis.game.equipment.Item;
import no.experis.game.equipment.Weapon;
import no.experis.game.equipment.itemEnums.ArmorType;
import no.experis.game.equipment.itemEnums.EquipmentSlot;
import no.experis.game.equipment.itemEnums.WeaponType;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;

import static org.junit.jupiter.api.Assertions.*;

class HeroTest {

    //Test that a character cannot equip a weapon that is too high level
    @Test
    public void equipWeaponWithTooHighLevelRequirement_checksThatACharacterCannotAWeaponWithALevelRequirementTheyDoNotMeet_shouldReturnInvalidWeaponException() {

        //Assign
        Warrior warrior = new Warrior("Test");
        Weapon weapon = new Weapon("Axe", 2, WeaponType.Axe, EquipmentSlot.Weapon,10,1);
        Item item = weapon;
        String expected = "You are too low level to equip this weapon";
        String actual = "";

        //Act
        Exception exception = assertThrows(InvalidWeaponException.class, () -> {
            warrior.equipWeapon(item);
        });
        actual = exception.getMessage();

        //Assert
        assertEquals(expected, actual);
    }


    //Test that a character cannot equip a armor that is too high level
    @Test
    public void equipArmorWithTooHighLevelRequirement_checksThatACharacterCannotEquipAArmorWithALevelRequirementTheyDoNotMeet_shouldReturnInvalidArmorException() {

        //Assign
        Warrior warrior = new Warrior("Test");
        PrimaryAttribute template = new PrimaryAttribute(2,2,2);
        Armor armor = new Armor("Plate armor", 2, EquipmentSlot.Body, ArmorType.Plate,10,template);
        Item item = armor;
        String expected = "You are too low level to equip this armor";
        String actual = "";

        //Act
        Exception exception = assertThrows(InvalidArmorException.class, () -> {
            warrior.equipArmor(item);
        });
        actual = exception.getMessage();

        //Assert
        assertEquals(expected, actual);
    }


    //Test that a character cannot equip a weapon type the is not in their WeaponTypes
    @Test
    public void equipWeaponOfInvalidType_checksThatACharacterCannotEquipAWeaponTypeTheyCannotEquip_shouldReturnInvalidWeaponException() {

        //Assign
        Warrior warrior = new Warrior("Test");
        Weapon weapon = new Weapon("Bow", 1, WeaponType.Bow, EquipmentSlot.Weapon,10,1);
        Item item = weapon;
        String expected = "You cannot equip this weapon type";
        String actual = "";

        //Act
        Exception exception = assertThrows(InvalidWeaponException.class, () -> {
            warrior.equipWeapon(item);
        });
        actual = exception.getMessage();

        //Assert
        assertEquals(expected, actual);
    }


    //Test that a character cannot equip a armor type the is not in their ArmorTypes
    @Test
    public void equipValidWeapon_checksThatACharacterCanEquipAValidWeapon_shouldReturnTrue() {

        //Assign
        Warrior warrior = new Warrior("Test");
        Weapon weapon = new Weapon("Axe", 1, WeaponType.Axe, EquipmentSlot.Weapon,10,1);
        Item item = weapon;
        boolean expected = true;
        boolean actual = false;

        //Act
        try {
            actual = warrior.equipWeapon(item);
        } catch (InvalidWeaponException e) {
            System.out.println(e);
        }

        //Assert
        assertEquals(expected, actual);
    }

    //Test that a character can equip a valid armor piece
    @Test
    public void equipValidArmor_checksThatACharacterCanEquipAValidArmor_shouldReturnTrue() {

        //Assign
        Warrior warrior = new Warrior("Test");
        PrimaryAttribute template = new PrimaryAttribute(2,2,2);
        Armor armor = new Armor("Plate armor", 1, EquipmentSlot.Body, ArmorType.Plate,10,template);
        Item item = armor;
        boolean expected = true;
        boolean actual = false;

        //Act
        try {
            actual = warrior.equipArmor(item);
        } catch (InvalidArmorException e) {
            System.out.println(e);
        }

        //Assert
        assertEquals(expected, actual);
    }


    //Test that a character can equip a armor type the is not in their ArmorTypes
    @Test
    public void equipArmorOfWrongType_checksThatACharacterCannotAArmorTypeTheyCannotEquip_shouldReturnInvalidArmorException() {
        //Assign
        Warrior warrior = new Warrior("Test");
        PrimaryAttribute template = new PrimaryAttribute(2,2,2);
        Armor armor = new Armor("Cloth robe", 1, EquipmentSlot.Body, ArmorType.Cloth,10,template);
        Item item = armor;
        String expected = "You cannot equip this armor type";
        String actual = "";

        //Act
        Exception exception = assertThrows(InvalidArmorException.class, () -> {
            warrior.equipArmor(item);
        });
        actual = exception.getMessage();

        //Assert
        assertEquals(expected, actual);
    }


    //Test that calculates a characters DPS with no weapon equipped
    @Test
    public void calculateDPSWithNoWeapon_calculatesACharactersDPSWithNoWeaponEquipped_shouldReturnCorrectDPS() {

        //Assign
        Warrior warrior = new Warrior("Test");
        double expected = BigDecimal.valueOf(1.0 * (1.0 + (5.0/100.0))).setScale(2, RoundingMode.CEILING).doubleValue();

        //Act
        double actual = warrior.calculateDPS();

        //Assert
        assertEquals(expected, actual);
    }


    //Test that calculates a characters DPS with a valid weapon equipped
    @Test
    public void calculateDPSWithValidWeapon_calculatesACharactersDPSWithValidWeaponEquipped_shouldReturnCorrectDPSWitValidWeapon() {

        //Assign
        Warrior warrior = new Warrior("Test");
        Weapon weapon = new Weapon("Axe", 1, WeaponType.Axe, EquipmentSlot.Weapon,7,1.1);
        Item item = weapon;
        try {
            warrior.equipWeapon(item);
        } catch (InvalidWeaponException e) {
            System.out.println(e);
        }

        double expected = BigDecimal.valueOf(7.0 * 1.1 * (1.0 + (5.0/100.0))).setScale(2, RoundingMode.CEILING).doubleValue();

        //Act
        double actual = warrior.calculateDPS();

        //Assert
        assertEquals(expected, actual);
    }


    //Test that calculates a characters DPS with a valid weapon equipped
    @Test
    public void calculateDPSWithValidWeaponAndArmor_calculatesACharactersDPSWithValidWeaponAndArmorEquipped_shouldReturnCorrectDPSWitValidWeaponAndArmor() {

        //Assign
        Warrior warrior = new Warrior("Test");
        Weapon weapon = new Weapon("Axe", 1, WeaponType.Axe, EquipmentSlot.Weapon,7,1.1);
        PrimaryAttribute template = new PrimaryAttribute(1,1,1);
        Armor armor = new Armor("Plate armor", 1, EquipmentSlot.Body, ArmorType.Plate,10,template);
        Item itemWeapon = weapon;
        Item itemArmor = armor;

        try {
            warrior.equipWeapon(itemWeapon);
        } catch (InvalidWeaponException e) {
            System.out.println(e);
        }
        try{
            warrior.equipArmor(itemArmor);
        } catch (InvalidArmorException e) {
            System.out.println(e);
        }

        double expected = BigDecimal.valueOf(7.0 * 1.1 * (1.0 + ((5.0 + 1.0)/100.0))).setScale(2, RoundingMode.CEILING).doubleValue();

        //Act
        double actual = warrior.calculateDPS();

        //Assert
        assertEquals(expected, actual);
    }
}