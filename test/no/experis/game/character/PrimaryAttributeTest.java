package no.experis.game.character;

import no.experis.game.equipment.Armor;
import no.experis.game.equipment.itemEnums.ArmorType;
import no.experis.game.equipment.itemEnums.EquipmentSlot;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PrimaryAttributeTest {

    //Testing that the updateAttribute() method updates the PrimaryAttributes with the new values
    @Test
    public void updateAttributes_updatesThePrimaryAttributeWithTheNewValue_shouldReturnUpdatedPrimaryAttributeValue() {

        //Assign
        PrimaryAttribute primaryAttribute = new PrimaryAttribute(3,3,3);
        PrimaryAttribute expectedAttributes = new PrimaryAttribute(5,5,5);
        String expected = expectedAttributes.textAttributes();

        //Act
        primaryAttribute.updateAttributes(5, 5, 5);
        String actual = primaryAttribute.textAttributes();

        //Assert
        assertEquals(expected, actual);
    }


    //Testing that the addAttributes() method adds a armors attributes to the PrimaryAttribute
    @Test
    public void addItemAttributesViaArmor_addsAArmorsAttributesToThePrimaryAttribute_shouldReturnUpdatedPrimaryAttributes() {

        //Assign
        PrimaryAttribute primaryAttribute = new PrimaryAttribute(3,3,3);
        PrimaryAttribute expectedAttributes = new PrimaryAttribute(5,5,5);
        String expected = expectedAttributes.textAttributes();

        PrimaryAttribute template = new PrimaryAttribute(2,2,2);
        Armor armor = new Armor("Test", 1,EquipmentSlot.Body, ArmorType.Leather,10,template);

        //Act
        primaryAttribute.addItemAttributes(armor);
        String actual = primaryAttribute.textAttributes();

        //Assert
        assertEquals(expected, actual);
    }


    //Testing that the removeAttributes() method removes a armors attributes from the PrimaryAttributes
    @Test
    public void removeItemAttributesViaArmor_removesAArmorsAttributesFromThePrimaryAttribute_shouldReturnUpdatedPrimaryAttributes() {

        //Assign
        PrimaryAttribute primaryAttribute = new PrimaryAttribute(5,5,5);
        PrimaryAttribute excpetedAttribute = new PrimaryAttribute(3,3,3);
        String expected = excpetedAttribute.textAttributes();

        PrimaryAttribute template = new PrimaryAttribute(2,2,2);
        Armor armor = new Armor("Test", 1,EquipmentSlot.Body, ArmorType.Leather,10,template);

        //Act
        primaryAttribute.removeItemAttributes(armor);
        String actual = primaryAttribute.textAttributes();

        //Assert
        assertEquals(expected, actual);
    }
}