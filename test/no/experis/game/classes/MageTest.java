package no.experis.game.classes;

import no.experis.game.character.PrimaryAttribute;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

//Test class that tests methods used to manipulate Mage objects
class MageTest {

    //Test that checks if a Mage is level 1 when created
    @Test
    public void correctStartingLevelMage_checksThatTheMageIsLevel1AfterCreation_shouldReturnThatTheMageIsLevel1() {

        //Assign
        Mage mage = new Mage ("Test");
        int expected = 1;

        //Act
        int actual = mage.getLevel();

        //Assert
        assertEquals(expected, actual);
    }


    //Test that checks that a Mage is level 2 when leveling up after creation
    @Test
    public void correctLevelAfterLevelUpMage_checksThatTheMageIsLevel2AfterLevelUp_shouldReturnThatTheMageIsLevel2() {

        //Assign
        Mage mage = new Mage ("Test");
        int expected = 2;

        //Act
        mage.levelUp();
        int actual = mage.getLevel();

        //Assert
        assertEquals(expected, actual);
    }


    //Test that checks that a Mage has the correct starting PrimaryAttributes
    @Test
    public void correctStartingPrimaryAttributesMage_checksThatTheMageHasTheCorrectStartingPrimaryAttributes_shouldReturnTheCorrectStartingPrimaryAttributes() {

        //Assign
        Mage mage = new Mage ("Test");
        PrimaryAttribute expectedAttributes = new PrimaryAttribute(1,1,8);
        String expected = expectedAttributes.textAttributes();

        //Act
        String actual = mage.getBasePrimaryAttribute().textAttributes();

        //Assert
        assertEquals(expected, actual);
    }


    //Test that checks that a Mage has its PrimaryAttributes increased as they levelUp
    @Test
    public void correctAttributesOnLevelUpMage_checksThatPrimaryAttributesAreIncreaseAtLevelUp_shouldReturnTheIncreasedPrimaryAttributes() {

        //Assign
        Mage mage = new Mage ("Test");
        PrimaryAttribute expectedAttributes = new PrimaryAttribute(2,2,13);
        String expected = expectedAttributes.textAttributes();

        //Act
        mage.levelUp();
        String actual = mage.getBasePrimaryAttribute().textAttributes();

        //Assert
        assertEquals(expected, actual);
    }
 }