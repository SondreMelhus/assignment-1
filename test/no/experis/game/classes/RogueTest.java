package no.experis.game.classes;

import no.experis.game.character.PrimaryAttribute;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

//Test class that tests methods used to manipulate Rogue objects
class RogueTest {

    //Test that checks if a Rogue is level 1 when created
    @Test
    public void correctStartingLevelRogue_checksThatTheRogueIsLevel1AfterCreation_shouldReturnThatTheRogueIsLevel1() {

        //Assign
        Rogue rogue = new Rogue ("Test");
        int expected = 1;

        //Act
        int actual = rogue.getLevel();

        //Assert
        assertEquals(expected, actual);
    }


    //Test that checks that a Rogue is level 2 when leveling up after creation
    @Test
    public void correctLevelAfterLevelUpRogue_checksThatTheRogueIsLevel2AfterLevelUp_shouldReturnThatTheRogueIsLevel2() {

        //Assign
        Rogue rogue = new Rogue ("Test");
        int expected = 2;

        //Act
        rogue.levelUp();
        int actual = rogue.getLevel();

        //Assert
        assertEquals(expected, actual);
    }


    //Test that checks that a Rogue has the correct starting PrimaryAttributes
    @Test
    public void correctStartingPrimaryAttributes_checksThatTheRogueHasTheCorrectStartingPrimaryAttributes_shouldReturnTheCorrectStartingPrimaryAttributesForRogue() {

        //Assign
        Rogue rogue = new Rogue ("Test");
        PrimaryAttribute expectedAttributes = new PrimaryAttribute(2,6,1);
        String expected = expectedAttributes.textAttributes();

        //Act
        String actual = rogue.getBasePrimaryAttribute().textAttributes();

        //Assert
        assertEquals(expected, actual);
    }


    //Test that checks that a Rogue has its PrimaryAttributes increased as they levelUp
    @Test
    public void correctPrimaryAttributesOnLevelUpRogue_checksThatPrimaryAttributesAreIncreasedAtLevelUp_shouldReturnTheIncreasedPrimarAttributes() {

        //Assign
        Rogue rogue = new Rogue ("Test");
        PrimaryAttribute expectedAttributes = new PrimaryAttribute(3,10,2);
        String expected = expectedAttributes.textAttributes();

        //Act
        rogue.levelUp();
        String actual = rogue.getBasePrimaryAttribute().textAttributes();

        //Assert
        assertEquals(expected, actual);
    }
}