package no.experis.game.classes;

import no.experis.game.character.PrimaryAttribute;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

//Test class that tests methods used to manipulate Warrior objects
class WarriorTest {

    //Test that checks if a Warrior is level 1 when created
    @Test
    void correctStartingLevelWarrior_checksThatTheWarriorIsLevel1AfterCreation_shouldReturnThatTheWarriorIsLevel1() {

        //Assign
        Warrior warrior = new Warrior ("Test");
        int expected = 1;

        //Act
        int actual = warrior.getLevel();

        //Assert
        assertEquals(expected, actual);
    }


    //Test that checks that a Warrior is level 2 when leveling up after creation
    @Test
    void correctLevelAfterLevelUpWarrior_checksThatTheWarriorIsLevel2AfterLevelUp_shouldReturnThatTheWarriorIsLevel2() {

        //Assign
        Warrior warrior = new Warrior ("Test");
        int expected = 2;

        //Act
        warrior.levelUp();
        int actual = warrior.getLevel();

        //Assert
        assertEquals(expected, actual);
    }


    //Test that checks that a Warrior has the correct starting PrimaryAttributes
    @Test
    void correctStartingPrimaryAttributesWarrior_checksThatTheWarriorHasTheCorrectStartingPrimaryAttributes_shouldReturnTheCorrectStartingPrimaryAttributes() {

        //Assign
        Warrior warrior = new Warrior ("Test");
        PrimaryAttribute expectedAttributes = new PrimaryAttribute(5,2,1);
        String expected = expectedAttributes.textAttributes();

        //Act
        String actual = warrior.getBasePrimaryAttribute().textAttributes();

        //Assert
        assertEquals(expected, actual);
    }


    //Test that checks that a Warrior has its PrimaryAttributes increased as they levelUp
    @Test
    public void correctPrimaryAttributesOnLevelUp_checksThatPrimaryAttributesAreIncreasedAtLevelUp_shouldReturnTheIncreasedPrimaryAttributes() {

        //Assign
        Warrior warrior = new Warrior ("Test");
        PrimaryAttribute expectedAttributes = new PrimaryAttribute(8,4,2);
        String expected = expectedAttributes.textAttributes();

        //Act
        warrior.levelUp();
        String actual = warrior.getBasePrimaryAttribute().textAttributes();

        //Assert
        assertEquals(expected, actual);
    }
}