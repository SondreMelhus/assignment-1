package no.experis.game.classes;

import no.experis.game.character.PrimaryAttribute;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

//Test class that tests methods used to manipulate Ranger objects
class RangerTest {

    //Test that checks if a Ranger is level 1 when created
    @Test
    public void correctStartingLevelRanger_checksThatTheRangerIsLevel1AfterCreation_shouldReturnThatTheRangerIsLevel1() {

        //Assign
        Ranger ranger = new Ranger ("Test");
        int expected = 1;

        //Act
        int actual = ranger.getLevel();

        //Assert
        assertEquals(expected, actual);
    }


    //Test that checks that a Ranger is level 2 when leveling up after creation
    @Test
    public void correctLevelAfterLevelUpRanger_checksThatTheRangerIsLevel2AfterLevelUp_shouldReturnThatTheRangerIsLevel2() {

        //Assign
        Ranger ranger = new Ranger ("Test");
        int expected = 2;

        //Act
        ranger.levelUp();
        int actual = ranger.getLevel();

        //Assert
        assertEquals(expected, actual);
    }


    //Test that checks that a Ranger has the correct starting PrimaryAttributes
    @Test
    public void correctStartingPrimaryAttributesRanger_checksThatTheRangerHasTheCorrectStartingPrimaryAttributes_shouldReturnTheCorrectStartingPrimaryAttributes() {

        //Assign
        Ranger ranger = new Ranger ("Test");
        PrimaryAttribute expectedAttributes = new PrimaryAttribute(1,7,1);
        String expected = expectedAttributes.textAttributes();

        //Act
        String actual = ranger.getBasePrimaryAttribute().textAttributes();

        //Assert
        assertEquals(expected, actual);
    }


    //Test that checks that a Ranger has its PrimaryAttributes increased as they levelUp
    @Test
    public void correctPrimaryAttributesOnLevelUpRanger_checksThatPrimaryAttributesAreIncreasedAtLevelUp_shouldReturnTheIncreasedPrimaryAttributes() {

        //Assign
        Ranger ranger = new Ranger ("Test");
        PrimaryAttribute expectedAttributes = new PrimaryAttribute(2,12,2);
        String expected = expectedAttributes.textAttributes();

        //Act
        ranger.levelUp();
        String actual = ranger.getBasePrimaryAttribute().textAttributes();

        //Assert
        assertEquals(expected, actual);
    }
}